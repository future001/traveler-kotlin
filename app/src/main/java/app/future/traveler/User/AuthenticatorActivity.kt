package app.future.traveler.User

import android.accounts.Account
import android.accounts.AccountAuthenticatorActivity
import android.accounts.AccountManager
import android.os.Bundle
import android.widget.Toast
import app.future.traveler.AccountAuthenticator
import app.future.traveler.Model.AuthModel
import app.future.traveler.Network.Fetch
import app.future.traveler.R
import kotlinx.android.synthetic.main.activity_authenticator.*
import android.content.Intent
import app.future.traveler.MainActivity


class AuthenticatorActivity : AccountAuthenticatorActivity () {
    val REGISTER_RESULT = 1
    private lateinit var accountManager: AccountManager
    private lateinit var fetch: Fetch
    private lateinit var login: String
    private lateinit var password: String
    private var authTokens: AuthModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authenticator)

        accountManager = AccountManager.get(baseContext)
        fetch = Fetch(this@AuthenticatorActivity)

        login_login_button.setOnClickListener{
           login()
        }

        login_register_link.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            this.startActivityForResult(intent, REGISTER_RESULT)
        }
    }

    private fun login() {
        login = login_login.text.toString()
        password = login_password.text.toString()

        if(login.isEmpty() || password.isEmpty()) {
            Toast.makeText(this@AuthenticatorActivity, R.string.fill_all_fields , Toast.LENGTH_SHORT).show()
            return
        }

        val intent = getIntent()

        val accountType = intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);
        val data = Bundle()

        authTokens = fetch.authTokens(login, password)

        if(authTokens == null) {
            Toast.makeText(this@AuthenticatorActivity, R.string.incorrect_credentials , Toast.LENGTH_SHORT).show()
            return
        }

        removeAccounts()

        val authToken = authTokens!!.accessToken
        val tokenType = authTokens!!.tokenType
        val refreshToken = authTokens!!.refreshToken

        data.putString(AccountManager.KEY_ACCOUNT_NAME, login)
        data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType)
        data.putString(AccountAuthenticator.KEY_TOKEN_TYPE, tokenType)
        data.putString(AccountManager.KEY_AUTHTOKEN, authToken)
        data.putString(AccountAuthenticator.REFRESH_TOKEN, refreshToken)

        intent.putExtras(data)
        setLoginResult(intent)
    }

    private fun setLoginResult(intent: Intent) {
        val account = Account(login, AccountAuthenticator.ACCOUNT_TYPE)
        val refreshToken = intent.getStringExtra(AccountAuthenticator.REFRESH_TOKEN)

        if (intent.getBooleanExtra(AccountAuthenticator.ADD_ACCOUNT, false)) {
            val authToken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN)
            val tokenType = intent.getStringExtra(AccountAuthenticator.KEY_TOKEN_TYPE)

            accountManager.addAccountExplicitly(account, refreshToken, null)
            accountManager.setAuthToken(account, tokenType, authToken)
        } else {
            accountManager.setPassword(account, refreshToken)
        }

        setAccountAuthenticatorResult(intent.getExtras())
        setResult(RESULT_OK, intent)

        finish()
    }

    private fun removeAccounts() {
        var accounts: Array<Account> = accountManager.getAccountsByType(AccountAuthenticator.ACCOUNT_TYPE)

        accounts.forEach {
            accountManager.removeAccount(it, null, null);
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (resultCode == RESULT_OK && requestCode == REGISTER_RESULT){
            Toast.makeText(this@AuthenticatorActivity, R.string.register_succeded , Toast.LENGTH_SHORT).show()

            login_login.setText(data.getStringExtra("username"))
            login_password.setText(data.getStringExtra("password"))
            login()
        } else
            super.onActivityResult(requestCode, resultCode, data)
    }
}
