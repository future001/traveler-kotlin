package app.future.traveler.User

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import app.future.traveler.Network.Fetch
import app.future.traveler.R
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        register_button.setOnClickListener{ register() }
        register_login_link.setOnClickListener{
            val intent = Intent(this, AuthenticatorActivity::class.java)
            startActivity(intent)
        }
    }

    private fun register() {
        val username = register_username.text.toString()
        val email = register_email.text.toString()
        val password = register_password.text.toString()
        val repeated_password = register_repeat_password.text.toString()

        if (username.length < 3 || password.length < 6 || repeated_password.length < 6) {
            Toast.makeText(this, R.string.register_fill_all_fields, Toast.LENGTH_SHORT).show()
            return
        }

        if (password != repeated_password) {
            Toast.makeText(this, R.string.passwords_not_match, Toast.LENGTH_SHORT).show()
            return
        }

        if(!isEmailValid(email)) {
            Toast.makeText(this, R.string.email_not_valid, Toast.LENGTH_SHORT).show()
            return
        }

        Fetch(this).register(username, email, password, {
            val data = Intent()
            data.putExtra("username", username)
            data.putExtra("password", password)
            setResult(Activity.RESULT_OK, data)
            finish()
        })
    }

    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}
