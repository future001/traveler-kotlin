package app.future.traveler.Interface

import app.future.traveler.Model.AuthModel
import app.future.traveler.Model.ThingModel
import app.future.traveler.Model.TravelModel
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @GET("api/travels")
    fun getTravels(@Header("Authorization") authorization: String): Call<List<TravelModel>>

    @GET("api/travel/{id}")
    fun getTravel(@Path(value = "id", encoded = true) id: String, @Header("Authorization") authorization: String): Call<TravelModel>

    @GET("oauth/v2/token")
    fun getToken(
            @Query("client_id") clientId: String,
            @Query("client_secret") clientSecret: String,
            @Query("grant_type") grantType: String,
            @Query("username") username: String,
            @Query("password") password: String
    ): Call<AuthModel>

    @GET("oauth/v2/token")
    fun refreshToken(
            @Query("client_id") clientId: String,
            @Query("client_secret") clientSecret: String,
            @Query("grant_type") grantType: String,
            @Query("refresh_token") refreshToken: String
    ): Call<AuthModel>

    @FormUrlEncoded
    @POST("api/travel/add")
    fun addTravel(
            @Header("Authorization") authorization: String,
            @Field("title") title: String ,
            @Field("startDate") startDate: String,
            @Field("endDate") endDate: String
            ): Call<Void>

    @DELETE("api/travel/remove/{id}")
    fun removeTravel(@Header("Authorization") authorization: String, @Path(value = "id", encoded = true) id: String): Call<Void>

    @GET("api/things/{travelId}")
    fun getThings(@Header("Authorization") authorization: String, @Path(value = "travelId", encoded = true) travelId: String): Call<List<ThingModel>>

    @FormUrlEncoded
    @POST("api/things/add/{travelId}")
    fun addThing(
            @Header("Authorization") authorization: String,
            @Field("name") title: String,
            @Path(value = "travelId", encoded = true) travelId: String
    ): Call<Void>

    @DELETE("api/things/remove/{id}")
    fun removeThing(@Header("Authorization") authorization: String, @Path(value = "id", encoded = true) id: String): Call<Void>

    @PUT("api/things/check/{id}")
    fun checkThing(@Header("Authorization") authorization: String, @Path(value = "id", encoded = true) id: String): Call<Void>

    @PUT("api/things/uncheck/{id}")
    fun uncheckThing(@Header("Authorization") authorization: String, @Path(value = "id", encoded = true) id: String): Call<Void>

    @FormUrlEncoded
    @POST("register")
    fun register(
            @Field("username") username: String ,
            @Field("email") email: String,
            @Field("plainpassword") plainpassword: String,
            @Field("client_id") clientId: String,
            @Field("client_secret") clientSecret: String
            ): Call<Void>
}