package app.future.traveler.Network

import android.accounts.Account
import android.accounts.AccountManager
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.widget.Toast
import app.future.traveler.AccountAuthenticator
import app.future.traveler.MainActivity
import app.future.traveler.Model.AuthModel
import app.future.traveler.Model.ThingModel
import app.future.traveler.Model.TravelModel
import app.future.traveler.User.AuthenticatorActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class Fetch(private val context: Context) {
    val NO_CONNECTION_ERROR = "Credentials invalid or server crash"
    // @TODO: przenieść id i secret do bezpieczniejszego miejsca
    val CLIENT_ID = "1_sYCEq25ECaPkvyvrLTJYB6cCDZJK5LtDByxA63M"
    val CLIENT_SECRET = "sLBCn32Q5rA2JYbs84XKbM9dPXTWEhF7ATk9Vbv3"
    val GRANT_TYPE_PASSWORD = "password"
    val GRANT_TYPE_REFRESH = "refresh_token"

    fun getTravels(account: Account, callback: (travels: ArrayList<TravelModel>) -> Unit, allowRefreshToken: Boolean = true) {
        val accountManager = AccountManager.get(context)
        val accessToken = accountManager.peekAuthToken(account, AccountAuthenticator.TOKEN_TYPE)
        val call: Call<List<TravelModel>> = Connector().getClient.getTravels(AccountAuthenticator.TOKEN_TYPE + " " + accessToken)
        val travels = ArrayList<TravelModel>()

        call.enqueue(object : Callback<List<TravelModel>> {
            override fun onResponse(call: Call<List<TravelModel>>, response: Response<List<TravelModel>>) {

                if (response.body() != null) {
                    travels.addAll(response.body()!!)
                    callback(travels)
                    return
                }

                if (response.message() == "Unauthorized" && allowRefreshToken) {
                    val tokenRefreshed = AccountAuthenticator(context).refreshAuthToken(account)
                    if (tokenRefreshed) {
                        getTravels(account, callback, false)
                    } else {
                        AccountAuthenticator(context).logout(account)
                    }
                    return
                }
                noConnectionError()
            }

            override fun onFailure(call: Call<List<TravelModel>>, t: Throwable) {
                noConnectionError()
            }
        })
    }

    fun getTravel(id: String, account: Account, callback: (travel: TravelModel) -> Unit, allowRefreshToken: Boolean = true) {
        val accountManager = AccountManager.get(context)
        val accessToken = accountManager.peekAuthToken(account, AccountAuthenticator.TOKEN_TYPE)
        val call: Call<TravelModel> = Connector().getClient.getTravel(id, AccountAuthenticator.TOKEN_TYPE + " " + accessToken)

        call.enqueue(object : Callback<TravelModel> {
            override fun onResponse(call: Call<TravelModel>, response: Response<TravelModel>) {
                if (response.body() != null) {
                    val travel: TravelModel = response.body()!!
                    callback(travel)
                    return
                }

                if (response.message() == "Unauthorized" && allowRefreshToken) {
                    val tokenRefreshed = AccountAuthenticator(context).refreshAuthToken(account)
                    if (tokenRefreshed) {
                        getTravel(id, account, callback, false)
                    } else {
                        AccountAuthenticator(context).logout(account)
                    }
                    return
                }
                noConnectionError()
            }

            override fun onFailure(call: Call<TravelModel>, t: Throwable) {
                noConnectionError()
            }
        })
    }

    fun addTravel(
            title: String,
            startDate: String,
            endDate: String,
            account: Account,
            callback: () -> Unit) {
        val accountManager = AccountManager.get(context)
        val accessToken = accountManager.peekAuthToken(account, AccountAuthenticator.TOKEN_TYPE)
        val call: Call<Void> = Connector().getClient.addTravel(AccountAuthenticator.TOKEN_TYPE + " " + accessToken, title, startDate, endDate)

        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                callback()
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                noConnectionError()
            }
        })
    }

    fun removeTravel(
            id: String,
            account: Account,
            callback: () -> Unit) {
        val accountManager = AccountManager.get(context)
        val accessToken = accountManager.peekAuthToken(account, AccountAuthenticator.TOKEN_TYPE)
        val call: Call<Void> = Connector().getClient.removeTravel(AccountAuthenticator.TOKEN_TYPE + " " + accessToken, id)

        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                callback()
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                noConnectionError()
            }
        })
    }

    fun authTokens(login: String, password: String): AuthModel? {
        lateinit var response: Response<AuthModel>

        val call: Call<AuthModel> = Connector().getClient.getToken(
                CLIENT_ID,
                CLIENT_SECRET,
                GRANT_TYPE_PASSWORD,
                login,
                password)

        val thread = Thread(Runnable {
            try {
                response = call.execute()
            } catch (e: Exception) {

            }
        })

        thread.start()
        thread.join()

        return response.body()
    }

    fun refreshToken(refreshToken: String): AuthModel? {
        var response: Response<AuthModel>? = null

        val call: Call<AuthModel> = Connector().getClient.refreshToken(
                CLIENT_ID,
                CLIENT_SECRET,
                GRANT_TYPE_REFRESH,
                refreshToken)

        val thread = Thread(Runnable {
            try {
                response = call.execute()
            } catch (e: Exception) {
            }
        })

        thread.start()
        thread.join()

        if (response == null) {
            return response
        } else {
            return response!!.body()
        }
    }

    fun getThings(travelId: String, account: Account, callback: (things: ArrayList<ThingModel>) -> Unit, allowRefreshToken: Boolean = true) {
        val accountManager = AccountManager.get(context)
        val accessToken = accountManager.peekAuthToken(account, AccountAuthenticator.TOKEN_TYPE)
        val call: Call<List<ThingModel>> = Connector().getClient.getThings(AccountAuthenticator.TOKEN_TYPE + " " + accessToken, travelId)
        val things = ArrayList<ThingModel>()

        call.enqueue(object : Callback<List<ThingModel>> {
            override fun onResponse(call: Call<List<ThingModel>>, response: Response<List<ThingModel>>) {
                if (response.body() != null) {
                    things.addAll(response.body()!!)
                    callback(things)
                    return
                }

                if (response.message() == "Unauthorized" && allowRefreshToken) {
                    val tokenRefreshed = AccountAuthenticator(context).refreshAuthToken(account)
                    if (tokenRefreshed) {
                        getThings(travelId, account, callback, false)
                    } else {
                        AccountAuthenticator(context).logout(account)
                    }
                    return
                }
                noConnectionError()
            }

            override fun onFailure(call: Call<List<ThingModel>>, t: Throwable) {
                noConnectionError()
            }
        })
    }

    fun addThing(
            name: String,
            travelId: String,
            account: Account,
            callback: () -> Unit) {
        val accountManager = AccountManager.get(context)
        val accessToken = accountManager.peekAuthToken(account, AccountAuthenticator.TOKEN_TYPE)
        val call: Call<Void> = Connector().getClient.addThing(AccountAuthenticator.TOKEN_TYPE + " " + accessToken, name, travelId)

        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                callback()
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                noConnectionError()
            }
        })
    }

    fun removeThing(
            id: String,
            account: Account,
            callback: () -> Unit) {
        val accountManager = AccountManager.get(context)
        val accessToken = accountManager.peekAuthToken(account, AccountAuthenticator.TOKEN_TYPE)
        val call: Call<Void> = Connector().getClient.removeThing(AccountAuthenticator.TOKEN_TYPE + " " + accessToken, id)

        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                callback()
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                noConnectionError()
            }
        })
    }

    fun checkThing(
            id: String,
            account: Account,
            callback: () -> Unit) {
        val accountManager = AccountManager.get(context)
        val accessToken = accountManager.peekAuthToken(account, AccountAuthenticator.TOKEN_TYPE)
        val call: Call<Void> = Connector().getClient.checkThing(AccountAuthenticator.TOKEN_TYPE + " " + accessToken, id)

        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                callback()
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                noConnectionError()
            }
        })
    }

    fun uncheckThing(
            id: String,
            account: Account,
            callback: () -> Unit) {
        val accountManager = AccountManager.get(context)
        val accessToken = accountManager.peekAuthToken(account, AccountAuthenticator.TOKEN_TYPE)
        val call: Call<Void> = Connector().getClient.uncheckThing(AccountAuthenticator.TOKEN_TYPE + " " + accessToken, id)

        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                callback()
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                noConnectionError()
            }
        })
    }

    fun register(
            username: String,
            email: String,
            password: String,
            callback: () -> Unit) {
        val call: Call<Void> = Connector().getClient.register(username, email, password, CLIENT_ID, CLIENT_SECRET)

        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                callback()
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                noConnectionError()
            }
        })
    }

    private fun noConnectionError() {
        Toast.makeText(context, NO_CONNECTION_ERROR, Toast.LENGTH_SHORT).show()
    }

}