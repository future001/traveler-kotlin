package app.future.traveler.Network

import app.future.traveler.Interface.ApiInterface
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Connector {
    private val API_URL = "http://192.168.21.1/oauth/traveller-server/public/index.php/"
//    private val API_URL = "http://192.168.99.1/traveler-server/public/index.php/"

    val getClient: ApiInterface
    get() {
        val gson = GsonBuilder()
                .setLenient()
                .create()

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

        return retrofit.create(ApiInterface::class.java)
    }
}