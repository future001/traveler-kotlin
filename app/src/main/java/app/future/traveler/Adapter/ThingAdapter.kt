package app.future.traveler.Adapter

import android.accounts.Account
import android.accounts.AccountManager
import android.graphics.Paint
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.future.traveler.AccountAuthenticator
import app.future.traveler.Model.ThingModel
import app.future.traveler.Network.Fetch
import app.future.traveler.R
import app.future.traveler.TravelActivity
import kotlinx.android.synthetic.main.travel_things_item.view.*

class ThingAdapter(private val things: ArrayList<ThingModel>, val travelActivity: TravelActivity) : RecyclerView.Adapter<ThingsViewHolder>() {
    private lateinit var account: Account;

    override fun getItemCount() = things.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThingsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellRow = layoutInflater.inflate(R.layout.travel_things_item, parent, false)

        val accountManager = AccountManager.get(travelActivity)
        var accounts: Array<Account> = accountManager.getAccountsByType(AccountAuthenticator.ACCOUNT_TYPE)
        account = accounts[0]

        return ThingsViewHolder(cellRow)
    }

    override fun onBindViewHolder(holder: ThingsViewHolder, position: Int) {
        holder.bindItems(things[position], this, position)
    }

    fun removeItem(id: String, position: Int) {
        Fetch(travelActivity).removeThing(id, account, {})

        things.removeAt(position)
        travelActivity.setThingsLabel()
        notifyDataSetChanged()
    }

    fun check(thingModel: ThingModel, position: Int) {
        val fetch = Fetch(travelActivity)

        if (thingModel.checked) {
            fetch.uncheckThing(thingModel.id!!, account, {
                things[position].checked = false
                notifyDataSetChanged()
            })
        } else {
            fetch.checkThing(thingModel.id!!, account, {
                things[position].checked = true
                notifyDataSetChanged()
            })
        }
    }
}

class ThingsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindItems(thingModel: ThingModel, adapter: ThingAdapter, position: Int) {
        try {
            itemView.things_name.text = thingModel.name

            if (thingModel.checked) {
                itemView.things_markAsChecked.setImageResource(R.drawable.uncheck)
                itemView.things_name.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                itemView.things_name.setTextColor(ContextCompat.getColor(adapter.travelActivity, R.color.colorLight))
            } else {
                itemView.things_markAsChecked.setImageResource(R.drawable.check)
                itemView.things_name.paintFlags = 0
                itemView.things_name.setTextColor(ContextCompat.getColor(adapter.travelActivity, R.color.colorWhite))
            }

            itemView.things_markAsChecked.setOnClickListener { adapter.check(thingModel, position) }
            itemView.things_removeThings.setOnClickListener { adapter.removeItem(thingModel.id!!, position) }
        } catch (e: Exception) {
        }
    }
}