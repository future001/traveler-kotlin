package app.future.traveler.Adapter

import android.accounts.Account
import android.accounts.AccountManager
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.future.traveler.AccountAuthenticator
import app.future.traveler.MainActivity
import app.future.traveler.Model.TravelModel
import app.future.traveler.Network.Fetch
import app.future.traveler.R
import app.future.traveler.TravelActivity
import kotlinx.android.synthetic.main.travels_item.view.*

class TravelAdapter(private val travelItems: ArrayList<TravelModel>, private val context: Context) : RecyclerView.Adapter<TravelViewHolder>() {

    override fun getItemCount() = travelItems.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TravelViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellRow = layoutInflater.inflate(R.layout.travels_item, parent, false)
        return TravelViewHolder(cellRow)
    }

    override fun onBindViewHolder(holder: TravelViewHolder, position: Int) {
        holder.bindItems(travelItems[position], this)
    }

    fun removeItem(id: String) {
        val accountManager = AccountManager.get(context)
        var accounts: Array<Account> = accountManager.getAccountsByType(AccountAuthenticator.ACCOUNT_TYPE)

        Fetch(context).removeTravel(id, accounts[0], {
            val intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)
        })
    }

    fun startTravelActivity(id:String?) {
        val intent = Intent(context, TravelActivity::class.java)
        intent.putExtra("id",  id)
        context.startActivity(intent)
    }
}

class TravelViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindItems(travelModel: TravelModel, adapter: TravelAdapter) {
        try {
            itemView.textView_title.text = travelModel.title
            itemView.textView_date.text = travelModel.startDate

            itemView.removeTravel.setOnClickListener{ adapter.removeItem(travelModel.id!!) }
            itemView.setOnClickListener{ adapter.startTravelActivity(travelModel.id) }
        } catch (e: Exception) {
        }
    }
}