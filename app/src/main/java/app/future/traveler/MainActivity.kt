package app.future.traveler

import android.accounts.Account
import android.accounts.AccountManager
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import app.future.traveler.Network.Fetch
import app.future.traveler.User.AuthenticatorActivity
import com.google.gson.Gson

class MainActivity : AppCompatActivity() {
    private val SPLASH_TIME: Long = 500
    private lateinit var fetch: Fetch
    private lateinit var accountManager: AccountManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()

        accountManager = AccountManager.get(this)
        var accounts: Array<Account> = accountManager.getAccountsByType(AccountAuthenticator.ACCOUNT_TYPE)

        if (accounts.isEmpty()) {
            startAuthenticatorActivity()
        } else {
            var accessToken = accountManager.peekAuthToken(accounts[0], AccountAuthenticator.TOKEN_TYPE)

            if (accessToken == null) {
                startAuthenticatorActivity()
            } else {
                fetch = Fetch(this@MainActivity)
                fetch.getTravels(accounts[0], {
                    Handler().postDelayed({
                        intent = Intent(this, TravelsActivity::class.java)
                        val jsonTravels = Gson().toJson(it)
                        intent.putExtra("travels", jsonTravels)

                        startActivity(intent)
                        finish()
                    }, SPLASH_TIME)
                })
            }
        }
    }

    private fun startAuthenticatorActivity(addAcount: Boolean = true) {
        val intent = Intent(this, AuthenticatorActivity::class.java)
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, AccountAuthenticator.ACCOUNT_TYPE)
        intent.putExtra(AccountAuthenticator.ADD_ACCOUNT, addAcount)

        startActivity(intent)
    }
}


// https://github.com/yuki24/android-exercises/blob/master/account-manager-example/src/net/yuki24/examples/AccountManager/core/ExampleAuthenticationService.java
// https://www.zoftino.com/android-account-manager-&-create-custom-account-type
// https://www.zoftino.com/how-to-get-auth-token-from-accountmanager-in-android