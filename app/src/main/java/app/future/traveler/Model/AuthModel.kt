package app.future.traveler.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AuthModel(

        @Expose
        @SerializedName("access_token")
        var accessToken: String? = null,

        @Expose
        @SerializedName("expires_in")
        var expiresIn: String? = null,

        @Expose
        @SerializedName("token_type")
        var tokenType: String? = null,

        @Expose
        @SerializedName("scope")
        var scope: String? = null,

        @Expose
        @SerializedName("refresh_token")
        var refreshToken: String? = null
        )