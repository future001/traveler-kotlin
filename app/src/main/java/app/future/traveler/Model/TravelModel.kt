package app.future.traveler.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class TravelModel(
        @Expose
        @SerializedName("id")
        var id: String? = null,

        @Expose
        @SerializedName("title")
        var title: String? = null,

        @Expose
        @SerializedName("start_date")
        var startDate: String? = null,

        @Expose
        @SerializedName("end_date")
        var endDate: String? = null,

        @Expose
        @SerializedName("things")
        var things: ArrayList<ThingModel> = ArrayList()
)
