package app.future.traveler.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ThingModel(

        @Expose
        @SerializedName("id")
        var id: String? = null,

        @Expose
        @SerializedName("name")
        var name: String? = null,

        @Expose
        @SerializedName("checked")
        var checked: Boolean = false)
