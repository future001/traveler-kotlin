package app.future.traveler

import android.accounts.AbstractAccountAuthenticator
import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.content.Context
import android.os.Bundle
import android.accounts.AccountManager.KEY_BOOLEAN_RESULT
import android.content.Intent
import android.text.TextUtils
import app.future.traveler.Network.Fetch
import app.future.traveler.User.AuthenticatorActivity
import app.future.traveler.Model.AuthModel


class AccountAuthenticator(private val context: Context) : AbstractAccountAuthenticator(context) {

    private var fetch: Fetch = Fetch(context)
    val accountManager = AccountManager.get(context)

    companion object{
        val PASSWORD = "password"
        val REFRESH_TOKEN = "refreshToken"
        val ADD_ACCOUNT = "addAccount"
        val KEY_TOKEN_TYPE = "tokenType"
        val TOKEN_TYPE = "Bearer"
        val ACCOUNT_TYPE = "future.traveler.auth"
    }

    override fun getAuthTokenLabel(p0: String?): String {
        return "full"
    }

    override fun confirmCredentials(p0: AccountAuthenticatorResponse?, p1: Account?, p2: Bundle?): Bundle? {
        return null
    }

    override fun updateCredentials(p0: AccountAuthenticatorResponse?, p1: Account?, p2: String?, p3: Bundle?): Bundle? {
        return null
    }

    override fun getAuthToken(response: AccountAuthenticatorResponse?, account: Account?, authTokenType: String?, options: Bundle?): Bundle {
        var authToken: String = accountManager.peekAuthToken(account, authTokenType)

        if(TextUtils.isEmpty(authToken)) {
            val password = accountManager.getPassword(account)

            if (password != null && account != null) {
                val authModel: AuthModel? = fetch.authTokens(account.name,  password)
                authToken = authModel!!.accessToken!!
            }
        }

        if (!TextUtils.isEmpty(authToken)) {
            val result = Bundle()
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account?.name)
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account?.type)
            result.putString(AccountManager.KEY_AUTHTOKEN, authToken)
            return result
        }

        val intent = Intent(context, AuthenticatorActivity::class.java)
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, account?.type)
        intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, account?.name)
        intent.putExtra(KEY_TOKEN_TYPE, authTokenType)

        val bundle = Bundle()
        bundle.putParcelable(AccountManager.KEY_INTENT, intent)

        return bundle
    }

    override fun hasFeatures(p0: AccountAuthenticatorResponse?, p1: Account?, p2: Array<out String>?): Bundle {
        val result =  Bundle()
        result.putBoolean(KEY_BOOLEAN_RESULT, false)
        return result
    }

    override fun editProperties(p0: AccountAuthenticatorResponse?, p1: String?): Bundle? {
        return null
    }

    override fun addAccount(response: AccountAuthenticatorResponse?, accountType: String?, authTokenType: String?, requiredFeatures: Array<out String>?, options: Bundle?): Bundle {
        val intent = Intent(context, AuthenticatorActivity::class.java)

        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, accountType)
        intent.putExtra(ADD_ACCOUNT, true)
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)

        val bundle = Bundle()
        bundle.putParcelable(AccountManager.KEY_INTENT, intent)

        return bundle
    }

    fun refreshAuthToken(account: Account): Boolean {
        val authModel: AuthModel? = fetch.refreshToken(accountManager.getPassword(account))

        if(authModel !== null) {
            accountManager.setPassword(account, authModel.refreshToken)
            accountManager.setAuthToken(account, TOKEN_TYPE, authModel.accessToken)

            return true
        }

        logout(account)
        return false
    }

    fun logout(account: Account) {
        accountManager.invalidateAuthToken(ACCOUNT_TYPE, accountManager.peekAuthToken(account, TOKEN_TYPE))
        accountManager.setPassword(account, "")

        val intent = Intent(context, MainActivity::class.java)
        context.startActivity(intent)
    }
}
