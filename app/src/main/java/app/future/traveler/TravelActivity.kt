package app.future.traveler

import android.accounts.Account
import android.accounts.AccountManager
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.widget.Toast
import app.future.traveler.Adapter.ThingAdapter
import app.future.traveler.Model.ThingModel
import app.future.traveler.Model.TravelModel
import app.future.traveler.Network.Fetch
import kotlinx.android.synthetic.main.activity_travel.*
import kotlinx.android.synthetic.main.add_thing_dialog.view.*

class TravelActivity : AppCompatActivity() {
    private lateinit var travel: TravelModel
    private lateinit var things: ArrayList<ThingModel>
    private lateinit var adapter: ThingAdapter
    private lateinit var account: Account

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_travel)

        val accountManager = AccountManager.get(this)
        var accounts: Array<Account> = accountManager.getAccountsByType(AccountAuthenticator.ACCOUNT_TYPE)
        account = accounts[0]

        val intent = intent
        val id = intent.getStringExtra("id")

        getTravel(id)
    }

    private fun getTravel(id: String) {
        Fetch(this@TravelActivity).getTravel(id, account, {
            travel = it
            things = travel.things
            setDataToView()
        })
    }

    private fun setDataToView() {
        travel_destination.text = travel.title
        travel_date.text = travel.startDate
        travel_days.text = travel.endDate.toString()

        travel_addThing.setOnClickListener {
            val dialogView = LayoutInflater.from(this).inflate(R.layout.add_thing_dialog, null)
            val dialogBuilder = AlertDialog.Builder(this)
                    .setView(dialogView)
                    .setTitle("Add thing")
            val dialog = dialogBuilder.show()

            dialogView.thingDialog_add.setOnClickListener {
                val name = dialogView.thingDialog_name.text.toString()

                if (name == "") {
                    Toast.makeText(this, "Fill all fields", Toast.LENGTH_SHORT).show()
                } else {
                    Fetch(this@TravelActivity).addThing(name, travel.id!!, account, {
                        dialog.dismiss()
                        updateThings()
                    })
                }
            }

            dialogView.thingDialog_cancel.setOnClickListener {
                dialog.dismiss()
            }
        }

        setDataToAdapter()
        setThingsLabel()
    }

    fun setThingsLabel() {
        if (things.isEmpty()) {
            travel_thingsLabel.text = "Your Backpack is empty"
        } else {
            travel_thingsLabel.text = "Your Backpack"
        }
    }

    private fun setDataToAdapter() {
        var linearLayoutManager = LinearLayoutManager(this)
        adapter = ThingAdapter(things, this)
        travel_things_recyclerView.layoutManager = linearLayoutManager
        travel_things_recyclerView.adapter = adapter
    }

    private fun updateThings() {
        Fetch(this@TravelActivity).getThings(travel.id!!, account, {
            things.clear()
            things.addAll(it)
            setThingsLabel()
            adapter.notifyDataSetChanged()
        })
    }
}
