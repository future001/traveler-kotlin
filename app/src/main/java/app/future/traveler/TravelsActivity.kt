package app.future.traveler

import android.accounts.Account
import android.accounts.AccountManager
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import app.future.traveler.Adapter.TravelAdapter
import app.future.traveler.Model.TravelModel
import app.future.traveler.Network.Fetch
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_travels.*
import kotlinx.android.synthetic.main.add_travel_dialog.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class TravelsActivity : AppCompatActivity() {
    private lateinit var travelItems: ArrayList<TravelModel>
    private lateinit var adapter: TravelAdapter
    private var calendar = Calendar.getInstance()
    private val DATE_FORMAT = "dd.MM.yyyy"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_travels)

        val intent: Intent = getIntent()
        val jsonTravels: String = intent.getStringExtra("travels")
        val type = object : TypeToken<ArrayList<TravelModel>>() {}.type

        travelItems = Gson().fromJson<ArrayList<TravelModel>>(jsonTravels, type)
        setDataToAdapter()

        travels_add_travel.setOnClickListener {
            val dialogView = LayoutInflater.from(this).inflate(R.layout.add_travel_dialog, null)
            val dialogBuilder = AlertDialog.Builder(this)
                    .setView(dialogView)
                    .setTitle("Add travel")
            val dialog = dialogBuilder.show()

            val dateStartListener = createDataSetListener(dialogView.travelDialog_startDate)
            val dateEndListener = createDataSetListener(dialogView.travelDialog_endDate)

            dialogView.travelDialog_startDate.setOnClickListener { createDatePickerDialog(dateStartListener) }
            dialogView.travelDialog_endDate.setOnClickListener { createDatePickerDialog(dateEndListener) }
            dialogView.travelDialog_add.setOnClickListener { addTravel(dialogView, dialog) }
            dialogView.travelDialog_cancel.setOnClickListener { dialog.dismiss() }
        }
    }

    private fun createDataSetListener(field: TextView): DatePickerDialog.OnDateSetListener {
        return DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateDateInDialog(field)
        }
    }

    private fun updateDateInDialog(field: TextView) {
        val dateFormatter = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())
        field.setText(dateFormatter.format(calendar.getTime()))
    }

    private fun createDatePickerDialog(dataSetListener: DatePickerDialog.OnDateSetListener) {
        return DatePickerDialog(this@TravelsActivity,
                dataSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show()
    }

    private fun addTravel(dialogView: View, dialog: Dialog) {

        val destination = dialogView.travelDialog_destination.text.toString()
        val startDate = dialogView.travelDialog_startDate.text.toString()
        val endDate = dialogView.travelDialog_endDate.text.toString()

        if (destination == "" || startDate == "" || endDate == "") {
            Toast.makeText(this, "Fill all fields", Toast.LENGTH_SHORT).show()
            return
        }

        dialog.dismiss()

        val accountManager = AccountManager.get(this)
        var accounts: Array<Account> = accountManager.getAccountsByType(AccountAuthenticator.ACCOUNT_TYPE)

        Fetch(this@TravelsActivity).addTravel(destination, startDate, endDate, accounts[0], {
            val intent  = Intent(this, MainActivity::class.java)
            startActivity(intent)
        })
    }

    private fun setDataToAdapter() {
        var linearLayoutManager = LinearLayoutManager(this)
        adapter = TravelAdapter(travelItems, this)
        travels_recyclerView.layoutManager = linearLayoutManager
        travels_recyclerView.adapter = adapter
    }
}
